import csv
import os
from datetime import datetime
from zipfile import ZipFile, is_zipfile

from flask import Flask, render_template, request

from fileops import FileOp

__author__ = "Tomas"
app = Flask(__name__)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

target = os.path.join(APP_ROOT, "archives/")
headers = "file,size,change\n"
DELIMITER = ","
OUTPUT_FILE = "archives/archive_content.csv"
DATE_FORMAT = "%Y-%m-%d %H:%M:%S"


def file_upload():
    """
    Uploads a given file to server as "archive.zip"
    """

    archive = request.files["file_name"]
    original_file_name = archive.filename
    destination = "/".join([target, "archive.zip"])
    archive.save(destination)

    if is_zipfile(destination):
        file_save_csv(destination)  # call function FILE SAVE TEMP CSV

    # print(original_file_name)
    return original_file_name


def file_save_csv(file_name):
    """
    Parses data from "archive.zip" and
    saves in CSV form into OUTPUT_FILE (*.csv)
    """
    with open(OUTPUT_FILE, "w") as f:
        ZIP = ZipFile(file_name)
        f.write(headers)  # write header to CSV

        # ZIP info parsing
        for file in ZIP.infolist():
            f_name = file.filename

            f_size = FileOp.convert_size(file.file_size)
            f_date = (datetime(*file.date_time[0:6])).strftime(DATE_FORMAT)

            f.write(
                f_name + DELIMITER + f_size + DELIMITER + f_date + "\n"
            )  # write rows to CSV

        # print("CSV done!")


def render_csv_to_html(original_file_name):
    """
    Reads temp CSV file and passes to output
    """
    if os.path.isfile("archives/archive_content.csv"):

        parse_csv()
        results = parse_csv()

        return render_template(
            "complete.html",
            file_upload="file uploaded successfully",
            results=results,
            original_file_name=original_file_name,
        )

    else:
        return render_template(
            "complete.html",
            file_upload="PROBLEM: you didn't pick a ZIP file",
            original_file_name="empty",
        )


def parse_csv():
    results = []
    with open("archives/archive_content.csv") as f:
        reader = csv.DictReader(f)  # read temp CSV
        # sorting by file name, case insensitive
        result = sorted(reader, key=lambda d: str(d["file"].lower()))

        for row in result:
            results.append(dict(row))  # dict for table output (POST page)

        return results


@app.route("/")
def index():
    return render_template("upload.html")


@app.route("/complete")
def complete():
    return render_template("complete.html")


@app.route("/upload", methods=["GET", "POST"])
def upload():
    # cleanup folder first, before use again
    FileOp.cleanup_temp_folder(target)

    if request.method == "GET":  # - for return from result page
        return render_template("upload.html")

    elif request.method == "POST":  # - for processing file

        FileOp.check_folder_presence(target)  # check if folder does exist

        file_upload()  # upload zip file to server
        original_file_name = file_upload()

        # render table to POST page
        return render_csv_to_html(original_file_name)


if __name__ == "__main__":
    app.run()
