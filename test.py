import os
import unittest

from werkzeug.datastructures import FileStorage

from app import app, target
from fileops import FileOp

app.config["TESTING"] = True


class FlaskTestCase(unittest.TestCase):
    def setUp(self):  # TESTS SETUP
        self.tester = app.test_client(self)

    #########################################

    def test_1_index(self):
        """
        TEST FLASK WORKS
        check for status code 200 in response
        """

        response = self.tester.get("/", content_type="html/text")
        self.assertEqual(response.status_code, 200)

    def test_2_upload_page(self):
        """
        TEST UPLOADS PAGE LOADS CORRECTLY
        - check if "Start with upload ZIP file" in response data
        """

        response = self.tester.get("/upload", content_type="html/text")
        self.assertTrue(b"Start with upload ZIP file" in response.data)

    def test_3_working_folder(self):
        """
        TEST WORKING FOLDER
        check if working folder does exist
        """

        response = FileOp.check_folder_presence(target)
        self.assertEqual(response, True)

    def test_4a_upload_known_file1_positive(self):
        """
        TEST UPLOAD KNOWN FILE
        upload "test_package1.ZIP", ensure there is "833" in response data
        """

        with open(os.path.join("samples", "test_package1.ZIP"), "rb") as fp:
            file = FileStorage(fp)

            response = self.tester.post(
                "/upload",
                data={"file_name": file},
                content_type="multipart/form-data",
                follow_redirects=True,
            )
            self.assertTrue(b"833" in response.data)

    def test_4b_upload_known_file1_negative(self):
        """
        TEST UPLOAD KNOWN FILE
        upload "test_package1.ZIP", ensure there is NOT "832" in response data
        """

        with open(os.path.join("samples", "test_package1.ZIP"), "rb") as fp:
            file = FileStorage(fp)

            response = self.tester.post(
                "/upload",
                data={"file_name": file},
                content_type="multipart/form-data",
                follow_redirects=True,
            )
            self.assertFalse(b"832" in response.data)

    def test_5a_upload_known_file2_positive(self):
        """
        TEST UPLOAD KNOWN FILE
        upload "test_package2.ZIP",
        ensure there is "l00000-100_asm" in response data
        """

        with open(os.path.join("samples", "test_package2.ZIP"), "rb") as fp:
            file = FileStorage(fp)

            response = self.tester.post(
                "/upload",
                data={"file_name": file},
                content_type="multipart/form-data",
                follow_redirects=True,
            )
            self.assertTrue(b"l00000-100_asm" in response.data)

    def test_5b_upload_known_file2_negative(self):
        """
        TEST UPLOAD KNOWN FILE
        upload "test_package2.ZIP",
        ensure there is NOT "l10000-100_asm" in response data
        """

        with open(os.path.join("samples", "test_package2.ZIP"), "rb") as fp:
            file = FileStorage(fp)

            response = self.tester.post(
                "/upload",
                data={"file_name": file},
                content_type="multipart/form-data",
                follow_redirects=True,
            )
            self.assertFalse(b"l10000-100_asm" in response.data)

    def test_6_not_zip_file_chosen(self):
        """
        TEST UPLOAD NOT ZIP FILE
        try to process when file isn't ZIP archive
        """

        with open(os.path.join("samples", "test_file.txt"), "rb") as fp:
            file = FileStorage(fp)

            response = self.tester.post(
                "/upload",
                data={"file_name": file},
                content_type="multipart/form-data",
                follow_redirects=True,
            )
            self.assertTrue(b"no file" in response.data)


if __name__ == "__main__":
    unittest.main()
