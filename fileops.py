import math
import os
import shutil


class FileOp:
    @staticmethod
    def convert_size(size_bytes):
        if size_bytes == 0:
            return "0 B"

        size_name = (" B", "kB", "MB", "GB", "TB")
        i = int(math.floor(math.log(size_bytes, 1024)))
        p = math.pow(1024, i)
        s = round(size_bytes / p, 2)
        return "%s %s" % (s, size_name[i])

    @staticmethod
    def cleanup_temp_folder(target):
        # cleanup folder from previous uploads
        # time.sleep(0.2) #delay 200ms before cleanup folder

        for filename in os.listdir(target):
            file_path = os.path.join(target, filename)

            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)

                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)

            except Exception as e:
                print("Failed to delete %s. Reason: %s" % (file_path, e))

    @staticmethod
    def check_folder_presence(target):
        # check if folder does exist
        if not os.path.isdir(target):
            os.mkdir(target)
            return False
        return True
