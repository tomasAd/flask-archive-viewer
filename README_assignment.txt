
Task is a "Archive previewer" web application written in python using flask web framework, that accepts a file processes it on the server and shows results of processing back to the user. Repository with the project should be hosted on the Gitlab and with Gitlab CI it should be deployed to heroku.com
Web applictation
Task is create a web-service to preview content of uploaded zip archives. It should con
Service should contain of 2 pages:

    Home page GET /
    With a single file select form and upload button for it. It should be only possible to select zip files.


    ---------------------- ---------------
    | Path ...           | | Select file |
    ---------------------- ---------------

    ----------
    | Upload |
    ----------

    Result page POST /
    On this page content of previously uploaded file is displayed in a table. At least path and size in human readable format should be shown.


        File: archive.zip

        Content:

        --------------------------
        | Path          | Size   |
        --------------------------
        | a.txt         | 20 Kb  |
        | b/c.doc       | 1 Mb   |
        --------------------------

Tests, CI and deployment
- Source code should be stored in a public repository on https://gitlab.com/
- Project repo should contain gitlab-ci.yml file with CI pipeline consisting of 2 states: test and deploy
- Tests should be written using either unittest or pytest. One test that uploads a known zipfile and checks the output of the POST endpoint is enough.

- During deploy stage, service should be deployed to heroku.


It's recommended to use Visual Studio Code or PyCharm Community Edition as development environment.
Free plans on both heroku and gitlab should be enough to finish this task.
Useful links

    https://docs.gitlab.com/ee/ci/examples/test-and-deploy-python-application-to-heroku.html
    https://docs.python.org/2/library/zipfile.html#zipfile.ZipFile.infolist
    https://blog.miguelgrinberg.com/post/handling-file-uploads-with-flask
    https://www.patricksoftwareblog.com/unit-testing-a-flask-application/

